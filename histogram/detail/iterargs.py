from collections import deque


class skippable:
    """Iterable wrapper to allow advancing in the body of the loop.

    Example usage::

        >>> for skip, item in skippable(range(10)):
        ...     print(item)
        ...     skip(3) # advance itr by 3
        ...
        0
        4
        8
    """

    def __init__(self, it):
        self.it = iter(it)

    def __iter__(self):
        return self

    def __next__(self):
        return self, next(self.it)

    def __call__(self, n):
        for _ in range(n):
            next(self.it, None)


def window(seq, size=2, wintype=tuple):
    """Moving-window iteration over a sequence.

    Example usage::

        >>> for items in window(range(5), size=3):
        ...     print(items)
        ...
        (0, 1, 2)
        (1, 2, 3)
        (2, 3, 4)
        (3, 4, None)
        (4, None, None)
    """
    assert size > 1
    it = iter(seq)
    items = deque((next(it, None) for _ in range(size)), maxlen=size)
    yield wintype(items)
    for item in it:
        items.append(item)
        yield wintype(items) if wintype else items
    for _ in range(size - 1):
        items.append(None)
        yield wintype(items) if wintype else items


if __name__ == '__main__':
    for skip, item in skippable(range(10)):
        print(item)
        skip(3) # advance itr by 3

    for items in window(range(5), size=3):
        print(items)
